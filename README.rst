####
pcmn
####

Simple declarative package management for Arch Linux.

Reasoning
=========

Declarative package management means essentially having a fixed list of packages
declared in a file used for installation. When running the package manager, all
packages in the list are installed and all installed packages not in the list
are removed. This allows you to keep better track of which packages are
installed and why, since you can reorganize the file any way you prefer and also
add comments to it.

This is heavily inspired by decpac_, but I wanted a more lightweight syntax on
the package list file.

Installation
============

Dependencies
------------

Check that you have the dependencies installed (otherwise ``pcmn`` will fail):

* ``python``
* ``pacman``
* ``yay``

Installing
----------

Install the package from AUR: pcmn-git_.

Usage
=====

When using ``pcmn`` for the first time, start by generating a package list::

    pcmn generate

This will create a package list in ``~/.config/pcmn/pkglist``. If you want to
save it to a different file, use ``--pkglist``.

Now, use the ``apply`` command anytime you want to apply the packages defined on
the package list. This will install packages listed there but not already
installed and remove the packages that are installed but aren't on the list::

    pcmn apply

Feel free to reorganize the packages order on the list, add comments, spacing,
etc. That's the point of using ``pcmn`` after all!

Package list format
-------------------

The formatting is very simple:

* One package per line
* Everything after a ``#`` is a comment
* A package can optionally be prefixed with a ``[GROUP]`` to assign it to the
  ``GROUP`` group, otherwise it will be assigned to the default group
  ``pacman``.
* Not every line needs to have a package (it can be empty, or have a comment)

Example::

    #
    #This is my package list
    #

    firefox # Need firefox to surf the web
    [aur] magic-wormhole # I use this to transfer files. Needs to be installed from AUR
    grub


Groups
------

A group can be used to define other commands to install, query and remove the
packages. Assigning a package to a group then makes it use the commands of its
group to be installed, query and removed.

The default group is ``pacman`` which uses pacman to query, install and remove
the native packages.

The other possible group is ``aur``, which is used for non-native packages, that
should be installed through AUR_. The ``yay`` AUR-helper is used for this.

Config file (changing commands)
-------------------------------

A configuration file can be used to change which commands are used to query,
install and remove packages from a specific group. It uses the JSON format and
by default is searched on ``~/.config/pcmn/config.json``. A sample JSON config
is in this repository as ``config.json``, and if you installed from the AUR, can
also be found at ``/usr/share/pcmn/config.json`` on your system.

Each command should be written as a list of strings. For ``install_cmd`` and
``remove_cmd``, if multiple commands are needed, a list of lists can be used
(just like in the sample config for ``remove_cmd``).

``query_cmd`` should return a list with only the package names installed for
that group and with each package in a separate line. Both ``install_cmd`` and
``remove_cmd`` are called with the packages to be installed/removed after the
last parameter.

Related projects
================

* decpac_: Very similar program since I used it as an inspiration for ``pcmn``.
* aconfmgr_: A more complete approach which also tracks configuration files.
  Great if you're not allergic to bash.

.. _AUR: https://aur.archlinux.org/
.. _decpac: https://github.com/rendaw/decpac
.. _aconfmgr: https://github.com/CyberShadow/aconfmgr
.. _pcmn-git: https://aur.archlinux.org/packages/pcmn-git/
